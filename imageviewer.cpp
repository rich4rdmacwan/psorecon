#include "imageviewer.h"
#include "ui_imageviewer.h"
#include <iostream>
#include <QDebug>
ImageViewer::ImageViewer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageViewer)
{
    ui->setupUi(this);
}

ImageViewer::~ImageViewer()
{
    delete ui;
}
//! Add images to the qpixmaps list which is used to match and display the two images. DEPRECATED
void ImageViewer::setImages(QStringList images)
{
    qpixmaps.clear();

    for(int i=0;i<images.count();i++){
        //qDebug()<<images[i];
        qpixmaps.push_back(QPixmap(images[i]));
    }
    updateView();
}
//! Add images to the qpixmaps list which is used to match and display the two images
void ImageViewer::setImages(std::vector<cv::Mat> images)
{
    for(int i=0;i<images.size();i++){
        //qDebug()<<images[i];
        qpixmaps.push_back(ASM::cvMatToQPixmap(images[i]));
    }
    updateView();
}
//!Sets the pixmaps to the labels and displays them
void ImageViewer::updateView()
{

    ui->lblImage->setPixmap(qpixmaps[0]);

    ui->lblImageMatch1->setPixmap(qpixmaps[1]);
    //qDebug()<<qpixmaps.size();
    if(qpixmaps.size()>2)
        ui->lblImageMatch2->setPixmap(qpixmaps[2]);

    update();
}
//! Adjusts size of the displayed images while resizing
void ImageViewer::resizeEvent(QResizeEvent *event)
{
    QDialog::resizeEvent(event);
    //    ui->lblImage->setPixmap(qpixmaps[0].scaled(ui->lblImage->width(),ui->lblImage->height()));
    //    ui->lblImageMatch1->setPixmap(qpixmaps[1].scaled(ui->lblImageMatch1->width(),ui->lblImageMatch1->height()));
    //    if(qpixmaps.size()>2)
    //        ui->lblImageMatch2->setPixmap(qpixmaps[2].scaled(ui->lblImageMatch2->width(),ui->lblImageMatch2->height()));
    QSize pixSize = ui->lblImage->pixmap()->size();
    pixSize.scale(ui->scrollImage->size(), Qt::KeepAspectRatio);
    ui->lblImage->setFixedSize(pixSize);

    pixSize = ui->lblImageMatch1->pixmap()->size();
    pixSize.scale(ui->scrollImageMatch1->size(), Qt::KeepAspectRatio);
    ui->lblImageMatch1->setFixedSize(pixSize);

    pixSize = ui->lblImageMatch2->pixmap()->size();
    pixSize.scale(ui->scrollImageMatch2->size(), Qt::KeepAspectRatio);
    ui->lblImageMatch2->setFixedSize(pixSize);
}
