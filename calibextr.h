#ifndef CALIBEXTR_H
#define CALIBEXTR_H
#include <cv.h>
#include "mat.h"
#include "mex.h"
#include <iostream>
#include <string>
#include <highgui.h>
#include <opencv2/features2d/features2d.hpp>
#include "opencv2/nonfree/nonfree.hpp"
using namespace cv;
//!
//! \brief The CalibExtr class
//! This class parses the .mat file and generates the structures in C++ containing the, E,F,camInfo,w_T_c structures alongwith an additional
//! convenience structure Intrinsics which is then used to generate the camera projection matrix structure, P.
//! <BR> NOTE: This class is a singleton.
class CalibExtr
{
    //! This function checks if the file exists on disk or not
public:
    inline bool fileExists(const std::string& name) {
        if (FILE *file = fopen(name.c_str(), "r")) {
            fclose(file);
            return true;
        } else {
            return false;
        }
    }

    /* Stores the path to the psoriasis images */
    std::string imagePath="/home/richard/Documents/Internship/3DReconstruction/data_psoriasis/";
    /* Stores the path to the PMVS (or any 3rd party binary to use for 3D reconstruction */
    std::string pmvsBinPath="/home/richard/Documents/Internship/Tools/CMVS-PMVS/build/main/";
    CalibExtr();
    cv::Mat E[21][21];
    cv::Mat F[21][21];
    cv::Mat w_T_c[21];
    struct Intrinsics{
        cv::Mat A[21];
        cv::Mat K[21];
    }Intrinsics;
    cv::Mat P[24][21];

    struct camInfo{
        cv::Mat pos[24][21];
        cv::Mat axes[24][21];
    }camInfo;

    void prepareForPMVS(std::string pmvsRootPath);
    void prepareForPMVS(std::string pmvsRootPath, std::string pmvsBinPath);
    static CalibExtr *extractCalibrationData(char* matFile);
    //Define the stereo pairs in an array for convenience.
    int stereoPairs[21][2]={{-1,11},{11,12},{12,13},{13,14},{14,15},{15,16},{16,17},
                            {17,18},{18,19},{19,20},{20,-1},{0,1},{1,2},{2,3},{3,4},
                            {4,5},{5,6},{6,7},{7,8},{8,9},{9,10} };
    /*
     *      0
     * 11
     *      1
     * 12
     *      2
     * 13
     *      3
     * 14
     *      4
     * 15
     *      5
     * 16
     *      6
     * 17
     *      7
     * 18
     *      8
     * 19
     *      9
     * 20
     *      10
     * */
//Options for pmvs
    int level;
    int csize;
    float threshold;
    int wsize;
    int minImageNum;
    int CPU;
    int useVisData;
    int sequence;
    std::string timages;
    int oimages;
private:
    static CalibExtr *calibExtr;
    int nCameras;
    int nRotations;
};

#endif // CALIBEXTR_H


