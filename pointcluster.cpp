#include "pointcluster.h"
#include <iostream>

GLfloat** PointCluster::getPoints() const
{
    return points;
}

void PointCluster::setPoints(GLfloat **value)
{
    points = value;
}

int PointCluster::getCount() const
{
    return count;
}

void PointCluster::setCount(int value)
{
    count = value;
}
PointCluster::PointCluster(int count)
{
 //   std::cout<<"Creating point cluster inzide";
    points=new GLfloat*[count];
    this->count=count;
   // std::cout<<"Finish Creating point cluster inzide";
}
