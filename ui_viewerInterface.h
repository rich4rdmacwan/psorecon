/********************************************************************************
** Form generated from reading UI file 'viewerInterface.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWERINTERFACE_H
#define UI_VIEWERINTERFACE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QVBoxLayout *verticalLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAr;
    QHBoxLayout *hboxLayout;
    QCheckBox *FPSCheckBox;
    QCheckBox *GridCheckBox;
    QCheckBox *AxisCheckBox;
    QSpacerItem *spacerItem;
    QPushButton *cancelButton;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->resize(657, 472);
        verticalLayout = new QVBoxLayout(Dialog);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        scrollArea = new QScrollArea(Dialog);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAr = new QWidget();
        scrollAr->setObjectName(QStringLiteral("scrollAr"));
        scrollAr->setGeometry(QRect(0, 0, 637, 413));
        scrollArea->setWidget(scrollAr);

        verticalLayout->addWidget(scrollArea);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setObjectName(QStringLiteral("hboxLayout"));
        FPSCheckBox = new QCheckBox(Dialog);
        FPSCheckBox->setObjectName(QStringLiteral("FPSCheckBox"));

        hboxLayout->addWidget(FPSCheckBox);

        GridCheckBox = new QCheckBox(Dialog);
        GridCheckBox->setObjectName(QStringLiteral("GridCheckBox"));

        hboxLayout->addWidget(GridCheckBox);

        AxisCheckBox = new QCheckBox(Dialog);
        AxisCheckBox->setObjectName(QStringLiteral("AxisCheckBox"));

        hboxLayout->addWidget(AxisCheckBox);

        spacerItem = new QSpacerItem(141, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

        hboxLayout->addItem(spacerItem);

        cancelButton = new QPushButton(Dialog);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));

        hboxLayout->addWidget(cancelButton);


        verticalLayout->addLayout(hboxLayout);


        retranslateUi(Dialog);
        QObject::connect(cancelButton, SIGNAL(clicked()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Interface", 0));
        FPSCheckBox->setText(QApplication::translate("Dialog", "FPS", 0));
        GridCheckBox->setText(QApplication::translate("Dialog", "Grid", 0));
        AxisCheckBox->setText(QApplication::translate("Dialog", "Axis", 0));
        cancelButton->setText(QApplication::translate("Dialog", "Quit", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWERINTERFACE_H
