#include "calibextr.h"
#include <fstream>

#ifdef _WIN32 || _WIN64
#include <windows.h>
#include <WinBase.h>
#else
#include <sys/sendfile.h>  // sendfile
#include <fcntl.h>         // open
#include <unistd.h>        // close
#include <sys/stat.h>      // fstat
#include <sys/types.h>     // fstat
#endif //WIN32

CalibExtr* CalibExtr::calibExtr=NULL;
CalibExtr::CalibExtr()
{
    level=4;
    csize=2;
    threshold=0.5;
    wsize=15;
    minImageNum=2;
    CPU =4;
    useVisData=1;
    sequence=-1;
    oimages=0;
    timages="-1 0 21";
}
//!
//! \brief CalibExtr::prepareForPMVS
//! \param pmvsRootPath points to the root folder of PMVS
//! This function prepares all the data for the PMVS binary to run the 3D reconstruction. The options.txt file, the vis.dat file,
//! the camera matrix text files in the txt folder, and the images in the visualize folder. It also creates the models folder required
//! for storing the results.
//!
void CalibExtr::prepareForPMVS(std::string pmvsRootPath)
{


    //If pmvsRootPath does not end with /, append a /
    std::string suffix="/";


    if (pmvsRootPath.size()>suffix.size() && !std::equal(suffix.rbegin(), suffix.rend(), pmvsRootPath.rbegin())){
        pmvsRootPath=pmvsRootPath+suffix;
    }

    std::string txtFolder= pmvsRootPath+std::string("txt/");
    std::string imgFolder= pmvsRootPath+std::string("visualize/");
    std::string modelsFolder= pmvsRootPath+std::string("models/");

    //Remove existing files
    std::string removeCommand="rm "+txtFolder+"* && rm "+imgFolder+"*";
    system(removeCommand.c_str());

    //Create directories if they do not exist
    mkdir(imgFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(txtFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(modelsFolder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);


    std::string visFileName=pmvsRootPath+std::string("vis.dat");
    std::string optionFileName=pmvsRootPath+std::string("option.txt");

    std::ofstream visFile,optionFile;


    visFile.open(visFileName.c_str());
      //Put in a loop for each rotation here

    //For each rotation, the images will change and the camera matrices will change
    //TODO: Calculate the camera matrices for the rotated position and save it in the file

    for(int rotationAngleIndex=0;rotationAngleIndex<nRotations;++rotationAngleIndex){
              for(int i=0;i<calibExtr->nCameras;i++){


            //Write into vis.dat i 2 stereoPairs[i][0] stereoPairs[i][1]
            if(stereoPairs[i][0]==-1 || stereoPairs[i][1]==-1)
                //TODO: 21*rotAngle should be added to each index to incorporate different rotations
                visFile<<21*rotationAngleIndex+i<<" 1 "<<((stereoPairs[i][0]>-1)?21*rotationAngleIndex+stereoPairs[i][0]:21*rotationAngleIndex+stereoPairs[i][1])<<"\n";
            else
                visFile<<21*rotationAngleIndex+i<<" 2 "<<21*rotationAngleIndex+stereoPairs[i][0]<<" "<<21*rotationAngleIndex+stereoPairs[i][1]<<"\n";
            char txtFilename[12],imageName[12],imageSuffix[19];

            //TODO: 21*rotAngle should be added to each index to incorporate different rotations
            sprintf(txtFilename,"%08d.txt",21*rotationAngleIndex+i);
            sprintf(imageName,"%08d.jpg",21*rotationAngleIndex+i);

            sprintf(imageSuffix,"cam_%02d/IMAGE_%04d.JPG",i,rotationAngleIndex);

            std::string srcImageName= calibExtr->imagePath+ std::string(imageSuffix);
            std::string destImageName= imgFolder+ std::string(imageName);
            std::string cameraMatrixFile=txtFolder+std::string(txtFilename);
            //std::ifstream cameraMatrixFile(txtFilename);
            if(!fileExists(destImageName)){
                //Copy the image file


                //Fast copy code. We cannot use this because images need to be rotated before saving. better use opencv
                    /*#ifdef _WIN32 || _WIN64
                    //Create directories if they do not exist
                    CreateDirectory(imgFolder.c_str(), NULL);
                    CreateDirectory(txtFolder.c_str(), NULL);
                    //Windows copy file
                    CopyFile(srcImageName,);
                    #else
                    //std::cout<<srcImageName<<std::endl;



                    std::cout<<"Copying image "<<imageName<<std::endl;
                    std::cout<<destImageName<<std::endl<<std::endl;
                        int source = open(srcImageName.c_str(), O_RDONLY, 0);
                        int dest = open(destImageName.c_str(), O_WRONLY | O_CREAT , 0644);
                        struct stat stat_source;
                        fstat(source, &stat_source);
                        std::cout<<stat_source.st_size<<" bytes"<<std::endl;
                        sendfile(dest, source, 0, stat_source.st_size);

                        close(source);
                        close(dest);
                        */

                std::cout<<"Preprocessing image "<<imageName<<" for pmvs "<<std::endl;
                cv::Mat src=imread(srcImageName);
                //rotate image according to the orientation
                transpose(src,src);
                if(i<11)
                    flip(src,src,1);
                else
                    flip(src,src,0);
                imwrite(destImageName,src);
//                #endif //WIN32
                //cv::Mat srcImage=imread(srcImageName);

            }

            if(!fileExists(cameraMatrixFile)){
                //cameraMatrixFile.open(sprintf);
                //Save camera matrices for each image in pmvsRootPath/txt folder

                std::ofstream file;
                file.open(cameraMatrixFile.c_str());
                file<<"CONTOUR\n";
                //TODO: 21*rotAngle should be added to each index to incorporate different rotations
                cv::Mat P=calibExtr->P[rotationAngleIndex][i];
                //TODO: Modify for corresponding rotation angle before writing

                for(int r=0;r<3;r++){
                    for(int c=0;c<4;c++){
                        file<<P.row(r).at<float>(c);
                        if(c<3) file<<" ";
                    }
                    if(r<2) file<<"\n";
                }
                file.close();
            }

          }
        }
    visFile.close();
    //TODO: Finally, prepare the options file to be used by pmvs2
    optionFile.open(optionFileName.c_str());
    optionFile<<"level "<<level<<"\n";
    optionFile<<"csize "<<csize<<"\n";
    optionFile<<"threshold "<<threshold<<"\n";
    optionFile<<"wsize "<<wsize<<"\n";
    optionFile<<"minImageNum "<<minImageNum<<"\n";
    optionFile<<"CPU "<<CPU<<"\n";
    optionFile<<"useVisData "<<useVisData<<"\n";
    optionFile<<"sequence "<<sequence<<"\n";
    std::stringstream timagesstr;
    timagesstr<<"-1 0 ";
    timagesstr<<nRotations*nCameras;
    optionFile<<"timages "<<timagesstr.str()<<"\n";

    mexPrintf("%s ",timagesstr.str().c_str());
    optionFile<<"oimages "<<oimages<<"\n";
    optionFile.close();
    //See for details of formatting the camera matrices and vis.dat file at http://www.di.ens.fr/pmvs/documentation.html
    //Now execute pmvs2
    std::string cmd=pmvsBinPath+"pmvs2 "+pmvsRootPath+std::string(" option.txt");
    std::cout<<cmd<<std::endl;

    system(cmd.c_str());
}

void CalibExtr::prepareForPMVS(std::string pmvsRootPath, std::string pmvsBinPath)
{
    this->pmvsBinPath=pmvsBinPath;
    prepareForPMVS(pmvsRootPath);
}
//!
//! \brief CalibExtr::extractCalibrationData
//! \param matFile is the path to the .mat file containing the camera parameters
//! \return instance of CalibExtr populated with data from the .mat file.
//!
CalibExtr* CalibExtr::extractCalibrationData(char* matFile)
{

    //Singleton instance
    if(calibExtr==NULL){
    calibExtr=new CalibExtr();
    calibExtr->nRotations=7;
    calibExtr->nCameras=21;

   MATFile *pmat;
    const char* name=NULL;
    mxArray *pa,*field;

    /* open mat file and read it's content */
    pmat = matOpen(matFile, "rw");
    if (pmat == NULL)
    {
        printf("Error Opening File: \"%s\"\n", matFile);
        return calibExtr;
    }

    /* Read in each array. */
    pa = matGetNextVariable(pmat, &name);
    int index=0;
    while (pa!=NULL)
    {
        /*
        * Diagnose array pa
        */
        printf("\nArray %s has %d dimensions.", name,
               mxGetNumberOfDimensions(pa));

        //print matrix elements
        //mlfPrintMatrix(pa);
        if(mxIsStruct(pa)){
            int nFields=mxGetNumberOfFields(pa);
            std::cout<<name<<" is a struct with "<<nFields<<" fields."<<std::endl;
            for(int i=0;i<nFields;i++){
                field=mxGetFieldByNumber(pa,index,i);
                std::string fieldName=std::string(mxGetFieldNameByNumber(pa,i));
                std::cout<<fieldName<<std::endl;
                //Extract E and F
                if(fieldName.compare("E")==0){
                    std::cout<<"Found E"<<std::endl;
                    mxArray* E=mxGetField(pa,index,fieldName.c_str());
                    int E_M=calibExtr->nCameras;
                    int E_N=calibExtr->nCameras;

                    //E_MxE_N=21x21
                    //for(int index=0;index>numEl;index++)
                    //Iterate through each cell of E and extract the 3x3 matrix
                    for(int e_m=0;e_m<E_M;e_m++){
                        for(int e_n=0;e_n<E_N;e_n++){
                        int index=21*e_m + e_n;

                        calibExtr->E[e_m][e_n]=cv::Mat(3,3,CV_32F);

                        //mexPrintf("\n");
                        mxArray* cell_element_ptr = mxGetCell(E, index);
                        bool emptyE=false;
                        if (cell_element_ptr == NULL) {
                          //mexPrintf("[%d] \n[]\n",index);
                            emptyE=true;
                        }
                        //else
                        {
                          /* Display a top banner. */
                          //mexPrintf("------------------------------------------------\n");
                          int M=3,N=3;

                          double* ptr=0;
                          if(!emptyE)
                            ptr=mxGetPr(cell_element_ptr);
                         for(int m=0;m<M;m++){
                            for(int n=0;n<N;n++){
                                //Extract the double data. Here it can be saved in 3d array.
                                if(!emptyE)
                                    calibExtr->E[e_m][e_n].at<float>(n,m)=*ptr++;
                                else
                                    calibExtr->E[e_m][e_n].at<float>(n,m)=0;
                                //mexPrintf("%f ",*ptr++);
                            }
                            //mexPrintf("\n");
                          }

                        }
                    }

                 }
                 //Display the whole E matrix similar to matlab style to compare
                 // for(int i=0;i<21;i++){
                 //    for(int j=0;j<21;j++){
                 //        cv::Mat currentE=matE[i][j];
                 //            cv::Scalar s =cv::sum(currentE);
                 //            if(s[0]==0){
                 //            mexPrintf("[]\n");
                 //                continue;
                 //            }
                 //            mexPrintf("ans =\n");
                 //            for(int r=0;r<3;r++){
                 //                for(int c=0;c<3;c++){
                 //                    mexPrintf("%f ",currentE.at<float>(r,c));
                 //                }
                 //                mexPrintf("\n");
                 //            }
                 //            mexPrintf("\n");
                 //        }
                 //    }
                }
                if(fieldName.compare("F")==0){
                    std::cout<<"Found F"<<std::endl;
                    mxArray* F=mxGetField(pa,index,fieldName.c_str());


                    int F_M=calibExtr->nCameras;
                    int F_N=calibExtr->nCameras;

                    //F_MxF_N=21x21
                    //for(int index=0;index>numEl;index++)
                    for(int f_m=0;f_m<F_M;f_m++){
                        for(int f_n=0;f_n<F_N;f_n++){
                        int index=calibExtr->nCameras*f_m + f_n;

                        calibExtr->F[f_m][f_n]=cv::Mat(3,3,CV_32F);

                        //mexPrintf("\n");
                        mxArray* cell_element_ptr = mxGetCell(F, index);
                        bool emptyF=false;
                        if (cell_element_ptr == NULL) {
                          //mexPrintf("[%d] \n[]\n",index);
                            emptyF=true;
                        }
                        //else
                        {
                          /* Display a top banner. */
                          //mexPrintf("------------------------------------------------\n");
                          int M=3,N=3;

                          double* ptr=0;
                          if(!emptyF)
                            ptr=mxGetPr(cell_element_ptr);
                         for(int m=0;m<M;m++){
                            for(int n=0;n<N;n++){
                                //Extract the double data. Here it can be saved in 3d array.
                                if(!emptyF)
                                    calibExtr->F[f_m][f_n].at<float>(n,m)=*ptr++;
                                else
                                    calibExtr->F[f_m][f_n].at<float>(n,m)=0;
                                //mexPrintf("%f ",*ptr++);
                            }
                            //mexPrintf("\n");
                          }

                        }
                    }

                 }
                 //Display the whole F matrix similar to matlab style to compare
                 // for(int i=0;i<21;i++){
                 //    for(int j=0;j<21;j++){
                 //        cv::Mat currentF=matF[i][j];
                 //            cv::Scalar s =cv::sum(currentF);
                 //            if(s[0]==0){
                 //            mexPrintf("[]\n");
                 //                continue;
                 //            }
                 //            mexPrintf("ans =\n");
                 //            for(int r=0;r<3;r++){
                 //                for(int c=0;c<3;c++){
                 //                    mexPrintf("%f ",currentF.at<float>(r,c));
                 //                }
                 //                mexPrintf("\n");
                 //            }
                 //            mexPrintf("\n");
                 //        }
                 //    }
                }
                if(fieldName.compare("w_T_c")==0){
                    std::cout<<"Found w_T_c"<<std::endl;
                 mxArray* W=mxGetField(pa,index,fieldName.c_str());

                    int ndims=mxGetNumberOfDimensions(W);
                    //mexPrintf("dims = %d",ndims);
                    int W_M=4;
                    int W_N=4;
                    int W_O=calibExtr->nCameras;

                    //F_MxF_N=21x21
                    //for(int index=0;index>numEl;index++)
                    double* ptr=mxGetPr(W);
                    for(int w_o=0;w_o<W_O;w_o++){
                      calibExtr->w_T_c[w_o]=cv::Mat(4,4,CV_32F);
                      // mexPrintf("\nans %d= \n\n",w_o);
                      for(int w_m=0;w_m<W_M;w_m++){
                            for(int w_n=0;w_n<W_N;w_n++){
                            calibExtr->w_T_c[w_o].at<float>(w_n,w_m)=*ptr++;
                            // mexPrintf("%f ",*ptr++);

                                }
                            //mexPrintf(" \n");
                            }

                 }
                 //Display the whole w_T_c matrix similar to matlab style to compare
                 // for(int i=0;i<21;i++){
                 //        cv::Mat currentW=matw_T_c[i];

                 //            mexPrintf("ans =\n");
                 //            for(int r=0;r<4;r++){
                 //                for(int c=0;c<4;c++){
                 //                    mexPrintf("%f ",currentW.at<float>(r,c));
                 //                }
                 //                mexPrintf("\n");
                 //            }
                 //            mexPrintf("\n");

                 //    }
                }
                if(fieldName.compare("camInfo")==0){
                    mxArray* caminfo=mxGetField(pa,index,fieldName.c_str());
                    std::cout<<"Found camInfo"<<std::endl;
                     for(int ii=0;ii<calibExtr->nCameras;++ii){
                        for(int jj=0;jj<calibExtr->nRotations;++jj){

                            mxArray* C=mxGetFieldByNumber(caminfo,24*ii+jj,0);
                            double* ptr=mxGetPr(C);

                            calibExtr->camInfo.pos[jj][ii]=cv::Mat(3,1,CV_32F);
                            calibExtr->camInfo.axes[jj][ii]=cv::Mat(3,3,CV_32F);
                            //Extract pos [3x1]
                            for(int iii=0;iii<3;++iii){
                                calibExtr->camInfo.pos[jj][ii].at<float>(iii,0)=*ptr++;
                              // mexPrintf("%f ",calibExtr->camInfo.pos[ii][jj].at<float>(iii,0));
                            }
                              // mexPrintf("\n============%d %d \n",ii,jj);
                               C=mxGetFieldByNumber(caminfo,24*ii+jj,1);
                               ptr=mxGetPr(C);
                            //Extract axes [3x3]
                            for(int iii=0;iii<3;++iii){
                                for(int jjj=0;jjj<3;++jjj){
                                //   mexPrintf("%f ",*ptr);
                                    calibExtr->camInfo.axes[jj][ii].at<float>(iii,jjj)=*ptr++;

                                }
                              // mexPrintf("\n");
                            }
                              // mexPrintf("---------------\n");
                        }

//                        for(int ii=0;ii<24;++ii){
//                            for(int jj=0;jj<21;++jj){
//                                cv::Mat pos=calibExtr->camInfo.pos[ii][jj];
//                                cv::Mat axes=calibExtr->camInfo.axes[ii][jj];
//                                //Extract pos [3x1]
//                                for(int iii=0;iii>3;++iii)
//                                    mexPrintf("%f ",calibExtr->camInfo.pos[ii][jj].at<float>(iii,1));
//                                mexPrintf("\n");
//                                //Extract axes [3x3]
//                                for(int iii=0;iii<3;++iii){
//                                    for(int jjj=0;jjj<3;++jjj){
//                                        mexPrintf("%f ",calibExtr->camInfo.axes[ii][jj].at<float>(iii,jjj));
//                                    }
//                                    mexPrintf("\n");
//                                }
//                                mexPrintf("-----------------\n");
//                            }
//                        }


                    }
                }
                if(fieldName.compare("Intrinsics")==0){
                    mexPrintf("Found Intrinsics\n");
                    mxArray* Intrinsics=mxGetField(pa,index,fieldName.c_str());
                    for(int ii=0;ii<calibExtr->nCameras;++ii){
                        //Extract A and K for each camera and store into calibExtr.Intrinsics
                        calibExtr->Intrinsics.A[ii]=cv::Mat(3,3,CV_32F);
                        calibExtr->Intrinsics.K[ii]=cv::Mat(5,1,CV_32F);
                        mxArray* intrA=mxGetFieldByNumber(Intrinsics,ii,0);
                        double* ptr=mxGetPr(intrA);
                        for(int r=0;r<3;r++){
                            for(int c=0;c<3;c++){
                                calibExtr->Intrinsics.A[ii].at<float>(c,r)=*ptr++;
                            }
                        }
                        mxArray* intrK=mxGetFieldByNumber(Intrinsics,ii,1);
                        ptr=mxGetPr(intrK);
                        for(int j=0;j<5;j++){
                                calibExtr->Intrinsics.K[ii].at<float>(j)=*ptr++;
                         }
                       }

                 for(int i=0;i<calibExtr->nRotations;++i){
                    for(int j=0;j<calibExtr->nCameras;++j){
                        mexPrintf("################################### %d,%d \n",i,j);
                        calibExtr->P[i][j]=cv::Mat(3,4,CV_32F);

                        cv::Mat temp=cv::Mat(3,4,CV_32F);

                        temp=calibExtr->w_T_c[j];

                        for(int c=0;c<3;++c){
                            temp.at<float>(c,3)=calibExtr->camInfo.pos[i][j].at<float>(c,0);

                           // mexPrintf("%f ",calibExtr->camInfo.pos[i][j].at<float>(c));
                            temp.at<float>(3,c)=0;
                        }

                          //mexPrintf("\n//////////\n");
                        for(int r=0;r<3;++r){
                            for(int c=0;c<3;++c){
                               //mexPrintf("%d ",);
                               temp.at<float>(c,r)=calibExtr->camInfo.axes[i][j].at<float>(r,c);
                              //mexPrintf("%f ",calibExtr->camInfo.axes[i][j].at<float>(r,c));
                            }
                            //mexPrintf("\n");
                        }

                        temp=temp.inv();
                        for(int r=0;r<4;++r){
                            for(int c=0;c<4;++c){
                                mexPrintf("%f ",temp.at<float>(r,c));
                            }
                            mexPrintf("\n");
                        }
                        calibExtr->P[i][j]=calibExtr->Intrinsics.A[j]* (temp(cv::Rect(0,0,4,3)));
                         mexPrintf("\n******************\n");
//                        for(int r=0;r<4;r++){
//                            for(int c=0;c<4;c++){
//                                std::cout<<calibExtr->P[i][j].at<float>(r,c)<<" ";
//                            }
//                            std::cout<<std::endl;
//                        }
//                        std::cout<<std::endl;
                    }
                  }

                }
            }

        }


        //get next variable
        pa = matGetNextVariable(pmat,&name);

        ++index;
        //destroy allocated matrix
        mxDestroyArray(pa);
    }

    matClose(pmat);

// for(int i=0;i<21;i++){
//                    for(int j=0;j<21;j++){
//                        cv::Mat currentE=calibExtr->E[i][j];
//                            cv::Scalar s =cv::sum(currentE);
//                            if(s[0]==0){
//                            mexPrintf("[]\n");
//                                continue;
//                            }
//                            mexPrintf("ans =\n");
//                            for(int r=0;r<3;r++){
//                                for(int c=0;c<3;c++){
//                                    mexPrintf("%f ",currentE.at<float>(r,c));
//                                }
//                                mexPrintf("\n");
//                            }
//                            mexPrintf("\n");
//                        }
//                    }
    }



    return calibExtr;
}
