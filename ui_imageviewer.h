/********************************************************************************
** Form generated from reading UI file 'imageviewer.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IMAGEVIEWER_H
#define UI_IMAGEVIEWER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "aspectratiopixmaplabel.h"

QT_BEGIN_NAMESPACE

class Ui_ImageViewer
{
public:
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollImageMatch1;
    QWidget *scrollAreaWidgetContents;
    AspectRatioPixmapLabel *lblImageMatch1;
    QScrollArea *scrollImageMatch2;
    QWidget *scrollAreaWidgetContents_3;
    AspectRatioPixmapLabel *lblImageMatch2;
    QScrollArea *scrollImage;
    QWidget *scrollAreaWidgetContents_2;
    AspectRatioPixmapLabel *lblImage;

    void setupUi(QDialog *ImageViewer)
    {
        if (ImageViewer->objectName().isEmpty())
            ImageViewer->setObjectName(QStringLiteral("ImageViewer"));
        ImageViewer->resize(826, 630);
        horizontalLayout = new QHBoxLayout(ImageViewer);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        scrollImageMatch1 = new QScrollArea(ImageViewer);
        scrollImageMatch1->setObjectName(QStringLiteral("scrollImageMatch1"));
        scrollImageMatch1->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 361, 300));
        lblImageMatch1 = new AspectRatioPixmapLabel(scrollAreaWidgetContents);
        lblImageMatch1->setObjectName(QStringLiteral("lblImageMatch1"));
        lblImageMatch1->setGeometry(QRect(10, 10, 341, 281));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lblImageMatch1->sizePolicy().hasHeightForWidth());
        lblImageMatch1->setSizePolicy(sizePolicy);
        lblImageMatch1->setProperty("scaledContents", QVariant(true));
        scrollImageMatch1->setWidget(scrollAreaWidgetContents);

        verticalLayout_3->addWidget(scrollImageMatch1);

        scrollImageMatch2 = new QScrollArea(ImageViewer);
        scrollImageMatch2->setObjectName(QStringLiteral("scrollImageMatch2"));
        scrollImageMatch2->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 361, 300));
        lblImageMatch2 = new AspectRatioPixmapLabel(scrollAreaWidgetContents_3);
        lblImageMatch2->setObjectName(QStringLiteral("lblImageMatch2"));
        lblImageMatch2->setGeometry(QRect(10, 10, 341, 281));
        sizePolicy.setHeightForWidth(lblImageMatch2->sizePolicy().hasHeightForWidth());
        lblImageMatch2->setSizePolicy(sizePolicy);
        scrollImageMatch2->setWidget(scrollAreaWidgetContents_3);

        verticalLayout_3->addWidget(scrollImageMatch2);


        horizontalLayout->addLayout(verticalLayout_3);

        scrollImage = new QScrollArea(ImageViewer);
        scrollImage->setObjectName(QStringLiteral("scrollImage"));
        scrollImage->setWidgetResizable(true);
        scrollAreaWidgetContents_2 = new QWidget();
        scrollAreaWidgetContents_2->setObjectName(QStringLiteral("scrollAreaWidgetContents_2"));
        scrollAreaWidgetContents_2->setGeometry(QRect(0, 0, 435, 610));
        lblImage = new AspectRatioPixmapLabel(scrollAreaWidgetContents_2);
        lblImage->setObjectName(QStringLiteral("lblImage"));
        lblImage->setGeometry(QRect(0, 0, 391, 601));
        sizePolicy.setHeightForWidth(lblImage->sizePolicy().hasHeightForWidth());
        lblImage->setSizePolicy(sizePolicy);
        lblImage->setProperty("scaledContents", QVariant(true));
        scrollImage->setWidget(scrollAreaWidgetContents_2);

        horizontalLayout->addWidget(scrollImage);

        horizontalLayout->setStretch(0, 5);
        horizontalLayout->setStretch(1, 6);

        retranslateUi(ImageViewer);

        QMetaObject::connectSlotsByName(ImageViewer);
    } // setupUi

    void retranslateUi(QDialog *ImageViewer)
    {
        ImageViewer->setWindowTitle(QApplication::translate("ImageViewer", "Dialog", 0));
        lblImageMatch1->setProperty("text", QVariant(QApplication::translate("ImageViewer", "TextLabel", 0)));
        lblImageMatch2->setProperty("text", QVariant(QApplication::translate("ImageViewer", "TextLabel", 0)));
        lblImage->setProperty("text", QVariant(QString()));
    } // retranslateUi

};

namespace Ui {
    class ImageViewer: public Ui_ImageViewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IMAGEVIEWER_H
