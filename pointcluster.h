#ifndef POINTCLUSTER_H
#define POINTCLUSTER_H
#include <GL/gl.h>
//!
//! \brief The PointCluster class
//! This class represents a set of points
class PointCluster
{
    GLfloat** points;
    int count;
public:
    PointCluster(int count);

    GLfloat** getPoints() const;
    void setPoints(GLfloat **value);
    int getCount() const;
    void setCount(int value);
};

#endif // POINTCLUSTER_H
