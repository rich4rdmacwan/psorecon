/****************************************************************************

 Copyright (C) 2002-2013 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.5.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License 
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain 
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

#include <qapplication.h>
#include "interface.h"
#include "ui_viewerInterface.h"
#include "calibextr.h"
/*! \mainpage 3D Reconstruction of psoriasis lesions
 *
 * \section intro_sec Introduction
 *
 This project aims at extracting 3D points from images of patients to visualize and quantize the psoriasis lesions
 */
class ViewerInterface : public QDialog, public Ui::Dialog
{
public:
	ViewerInterface() { setupUi(this); }
};

int main(int argc, char** argv)
{
  QApplication application(argc,argv);

  ViewerInterface vi;
  //Extract calibration data
  CalibExtr* calibExtr=CalibExtr::extractCalibrationData("calibExtrIntr.mat");

  //Prepare data for pmvs2. This takes quite a long time to execute. If successful, this should store a .ply file which can be used by
  //the following code later.
  //calibExtr->prepareForPMVS("/home/richard/Documents/Internship/3DReconstruction/psorecon/pmvsData/");


  //The following code sets up the QGLViewer to show custom 3D points. This is still experimental.
  //TODO: After the correct 3D points are generated from line 42, the .ply file can be parsed to extract the 3D points and pass them
  //to the QGLViewer. The class PointCluster can be used to represent multiple points. (Around a million points can be displayed without affecting
  //the performance).

  Viewer viewer(&vi,calibExtr);
  vi.scrollArea->setWidget(&viewer);

  vi.setWindowTitle("Psoriasis Reconstruction");

  vi.show();
  qRegisterMetaType<PointCluster*>("PointCluster");


  return application.exec();
}
