/****************************************************************************

 Copyright (C) 2002-2013 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.5.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License 
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain 
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/
#include "calibextr.h"
#include <QGLViewer/qglviewer.h>
#include <vector>
#include <QRunnable>
#include <math.h>
#include "processor.h"
class Viewer : public QGLViewer
{
Q_OBJECT
public :
  Viewer(QWidget *parent);
  Viewer(QWidget *parent,CalibExtr* calibExtr);

protected :
  virtual void draw();
  virtual void postDraw();
  virtual QString helpString() const;
  virtual void init();
  CalibExtr* calibExtr;
  void checkCalibData(CalibExtr* calibExtr);
public Q_SLOTS:
  void newPointClusterAdded(PointCluster*);
private:
  void drawCornerAxis();
  void drawPoint(GLfloat x,GLfloat y,GLfloat z,GLfloat radius, GLint subdivisions,GLUquadric* quadric);
  GLfloat **points;
  int nPoints;
  std::vector<PointCluster*> allPoints;

  /*!
   * \brief updatingPoints: Flag to manage asynchronous drawing.
   * 3D points would be drawn only when this flag is disabled.
   */
  bool updatingPoints;

    Processor p;

};
