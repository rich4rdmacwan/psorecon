#include "processor.h"
#include<QDebug>
Processor::Processor(QObject *parent) :
    QObject(parent)
{
    init();
}
/*!
 * \brief Processor::init
 * Initializes things. Especially, loads calibration data from a mat file
 */
void Processor::init()
{
    calibExtr=CalibExtr::extractCalibrationData("calibExtr.mat");
    nPoints=0;
}
/*!
 * \brief Processor::process
 * Does all the processing. For each of the 24 orientations, triangulates the 3d points
 * using the correct stereo pair for each of the 21 cameras. Periodically updates the point
 * cloud.
 */
void Processor::process()
{
    QThreadPool* pool=QThreadPool::globalInstance();

    setAutoDelete(false);
    pool->start(this);
    //run();

}
/*!
 * \brief Processor::run
 * Do the point extraction and triangulation in a seperate thread. We don't have to wait for the GUI to show
 * up until all points are triangulated.
 */
void Processor::run()
{
    //For each rotation
    std::cout<<"Running "<<std::endl;
    for(int i=0;i<1;i++){
        //For each camera. We only iterate upto camera 10 since all the other cameras(11-20) appear
        //as pairs of the ones from 0 to 10. For now skip cam 0 because of wrong calibration
        for(int j=1;j<2;j++){

            //Find stereo pair images of amera j corresponding to rotation i,Two pairs for each camera
            preprocessImages(i,j);

            //TODO: Extract features from the two stereo pairs
            //TODO: Find matching points
            resize(imageMatsGray[0],imageMatsGray[0],Size(600,800));
            resize(imageMatsGray[1],imageMatsGray[1],Size(600,800));
            matchPairs(imageMatsGray[0],imageMatsGray[1]);

            imgViewer.setImages(imageMatsGray);
            imgViewer.show();
            //TODO: Perform triangulation to estimate 3D points
            //Test points

            float r1=0,r2=0,r3=0;
            nPoints=100000;
            //std::cout<<"Creating point cluster"<<std::endl;
            PointCluster* pc=new PointCluster(nPoints);
            //std::cout<<"count="<<pc->getCount()<<std::endl;
            for(int i=0;i<pc->getCount();i++){

                pc->getPoints()[i]=new GLfloat[3];
                r1 = -1+static_cast <float> (rand()) / static_cast <float> (RAND_MAX)*2;
                r2 = -1+static_cast <float> (rand()) / static_cast <float> (RAND_MAX)*2;
                r3 = -1+static_cast <float> (rand()) / static_cast <float> (RAND_MAX)*2;

                *(*(pc->getPoints()+i))=r1;
                *(*(pc->getPoints()+i)+1)=r2;
                *(*(pc->getPoints()+i)+2)=r3;
            }

            //TODO: Update 3D point cloud by adding this batch of points.
            addNewPointCluster(pc);
            QThread::msleep(1000);
        }
    }
}

QStringList Processor::getStereoPairImages(int index)
{
    //std::cout<<j<<"-->"<<stereoPairs[j][0]<<","<<stereoPairs[j][1]<<std::endl;
    QString match1="cam_"+QString::number(calibExtr->stereoPairs[index][0]);
    QString match2="cam_"+QString::number(calibExtr->stereoPairs[index][1]);
    QString original=(index<10?"cam_0"+QString::number(index):"cam_"+QString::number(index));
    QStringList list;
    list<<original;
    if(calibExtr->stereoPairs[index][0]>0)
        list<<match1;
    if(calibExtr->stereoPairs[index][1]>0)
        list<<match2;
    return list;
}

//! Preprocess the images to orientate correctly
void Processor::preprocessImages(int i,int j)
{
    imageMats.clear();
    imageMatsGray.clear();
    QStringList list=getStereoPairImages(j);
    QString imgSuffix=(i<10)?"/IMAGE_000"+QString::number(i):"/IMAGE_00"+QString::number(i);

    for(int n=0;n<list.count();n++){
        list[n]=QString::fromStdString(calibExtr->imagePath)+list[n]+imgSuffix+".JPG";
        qDebug()<<list[n];
        imageMats.push_back(imread(list[n].toStdString().c_str()));
    }

    //Rotate the images so that they have the same orientation
    transpose(imageMats[0],imageMats[0]);
    flip(imageMats[0],imageMats[0],1);
    //Save grayscale images
    imageMatsGray.push_back(imageMats[0].clone());
    cvtColor(imageMats[0],imageMatsGray[0],CV_BGR2GRAY);

    transpose(imageMats[1],imageMats[1]);
    flip(imageMats[1],imageMats[1],0);
    imageMatsGray.push_back(imageMats[1].clone());
    cvtColor(imageMats[1],imageMatsGray[1],CV_BGR2GRAY);

    if(imageMats.size()>2){
        transpose(imageMats[2],imageMats[2]);
        flip(imageMats[2],imageMats[2],0);

        imageMatsGray.push_back(imageMats[2].clone());
        cvtColor(imageMats[2],imageMatsGray[2],CV_BGR2GRAY);
    }




}

//!
//! \brief Processor::matchPairs
//! \param img1
//! \param img2
//! Find matching points between two images
void Processor::matchPairs(Mat &img1, Mat &img2)
{
    SurfFeatureDetector detector(400);
       vector<KeyPoint> keypoints1, keypoints2;
       detector.detect(img1, keypoints1);
       detector.detect(img2, keypoints2);

       // computing descriptors
       SurfDescriptorExtractor extractor;
       Mat descriptors1, descriptors2;
       extractor.compute(img1, keypoints1, descriptors1);
       extractor.compute(img2, keypoints2, descriptors2);

       flann::KDTreeIndexParams index_params(5);
       flann::SearchParams search_params(40);

       FlannBasedMatcher flannMatcher;
        float nndrRatio = 0.7f;
//       // matching descriptors
//       BFMatcher matcher(NORM_L2);
        std::vector< vector< DMatch >  > matches;
       //matcher.match(descriptors1, descriptors2, matches);
       flannMatcher.knnMatch(descriptors1,descriptors2,matches,2);

       vector< DMatch > good_matches;
        good_matches.reserve(matches.size());

        for (size_t i = 0; i < matches.size(); ++i)
        {
            if (matches[i].size() < 2)
                        continue;

            const DMatch &m1 = matches[i][0];
            const DMatch &m2 = matches[i][1];

            if(m1.distance <= nndrRatio * m2.distance)
            good_matches.push_back(m1);
        }

       // namedWindow("matches", 1);
       Mat img_matches;
       drawKeypoints(img1,keypoints1, img1);
       drawKeypoints(img2,keypoints2, img2);
       if( (good_matches.size() >=7))
         {

           std::cout << "OBJECT FOUND!" << std::endl;

           std::vector< Point2f > obj;
           std::vector< Point2f > scene;

           for( unsigned int i = 0; i < good_matches.size(); i++ )
           {
               //-- Get the keypoints from the good matches
               obj.push_back( keypoints1[ good_matches[i].queryIdx ].pt );
               scene.push_back( keypoints2[ good_matches[i].trainIdx ].pt );
           }

           Mat H = findHomography( obj, scene, CV_RANSAC,8);


           Mat objectMat=img1;

           //-- Get the corners from the image_1 ( the object to be "detected" )
           std::vector< Point2f > obj_corners(4);
           obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint( objectMat.cols, 0 );
           obj_corners[2] = cvPoint( objectMat.cols, objectMat.rows ); obj_corners[3] = cvPoint( 0, objectMat.rows );
           std::vector< Point2f > scene_corners(4);

           perspectiveTransform( obj_corners, scene_corners, H);


           //-- Draw lines between the corners (the mapped object in the scene - image_2 )
//           Scalar color(255,255,255);
//           line( img1, scene_corners[0] , scene_corners[1], color, 2 ); //TOP line
//           line( img1, scene_corners[1] , scene_corners[2], color, 2 );
//           line( img1, scene_corners[2] , scene_corners[3], color, 2 );
//           line( img1, scene_corners[3] , scene_corners[0] , color, 2 );

//           line( img2, obj_corners[0] , obj_corners[1], color, 2 ); //TOP line
//           line( img2, obj_corners[1] , obj_corners[2], color, 2 );
//           line( img2, obj_corners[2] , obj_corners[3], color, 2 );
//           line( img2, obj_corners[3] , obj_corners[0] , color, 2 );

         }
         else {
             std::cout << "OBJECT NOT FOUND!" << std::endl;
         }


           std::cout << "Matches found: " << matches.size() << std::endl;
           std::cout << "Good matches found: " << good_matches.size() << std::endl;
           Mat outimg;
        drawMatches(img1,keypoints1,img2,keypoints2,good_matches,outimg);
        imshow("",outimg);
        waitKey(0);


}
