/****************************************************************************

 Copyright (C) 2002-2013 Gilles Debunne. All rights reserved.

 This file is part of the QGLViewer library version 2.5.2.

 http://www.libqglviewer.com - contact@libqglviewer.com

 This file may be used under the terms of the GNU General Public License
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain
 additional rights, described in the file GPL_EXCEPTION in this package.

 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.

 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*****************************************************************************/

#include "interface.h"
#include <QThreadPool>

// Constructor must call the base class constructor.
Viewer::Viewer(QWidget *parent)
    : QGLViewer(parent)
{

    restoreStateFromFile();
    updatingPoints=true;
    connect(&p,SIGNAL(addNewPointCluster(PointCluster*)),this,SLOT(newPointClusterAdded(PointCluster*)));


}
//!
//! \brief Viewer::Viewer
//! \param parent
//! \param calibExtr is used to set the calibExtr structure to the instance variable
//!
Viewer::Viewer(QWidget *parent, CalibExtr *calibExtr)
    : QGLViewer(parent)
{
    restoreStateFromFile();
    updatingPoints=true;

    connect(&p,SIGNAL(addNewPointCluster(PointCluster*)),this,SLOT(newPointClusterAdded(PointCluster*)));
    this->calibExtr=calibExtr;

}
//!Init function to initialize stuff
/*!
 * \brief Viewer::init. Extracts calibration data from mat file.
*/
void Viewer::init()
{
    //TODO: Serialize calibExtr so that we dont need the matlab file or installation always
    calibExtr=CalibExtr::extractCalibrationData("calibExtr.mat");

 //   QGLViewer::init();
    //checkCalibData(calibExtr);
    p.process();

}
//!
//! \brief Viewer::checkCalibData
//! \param calibExtr
//! Test function to check the structure calibExtr's integrity
void Viewer::checkCalibData(CalibExtr *calibExtr)
{
    //For now, it just prints the calibration data.
    //TODO: Make it return boolean and display an error message and stop if this data is not valid
    //Check if CalibExtr is proper
     for(int i=0;i<21;i++){
        for(int j=0;j<21;j++){
            cv::Mat currentE=calibExtr->F[i][j];
                cv::Scalar s =cv::sum(currentE);
                if(s[0]==0){
                mexPrintf("[]\n");
                    continue;
                }
                mexPrintf("ans =\n");
                for(int r=0;r<3;r++){
                    for(int c=0;c<3;c++){
                        mexPrintf("%f ",currentE.at<float>(r,c));
                    }
                    mexPrintf("\n");
                }
                mexPrintf("\n");
            }
        }

}
//!
//! \brief Viewer::newPointClusterAdded
//! \param pointCluster
//! This slot is triggered when a new pointcluster is added to the allPoints vector.
void Viewer::newPointClusterAdded(PointCluster *pointCluster)
{
   updatingPoints=true;
    //std::cout<<"New points added"<<std::endl;
    allPoints.push_back(pointCluster);

   updatingPoints=false;
   updateGL();
}

//!
//! \brief Viewer::drawCornerAxis
//! This function draws the coordinate axes in the bottom left corner
void Viewer::drawCornerAxis()
{
    int viewport[4];
    int scissor[4];
    GLfloat axisLength=0.5;

    // The viewport and the scissor are changed to fit the lower left
    // corner. Original values are saved.
    glGetIntegerv(GL_VIEWPORT, viewport);
    glGetIntegerv(GL_SCISSOR_BOX, scissor);

    // Axis viewport size, in pixels
    const int size = 150;
    glViewport(0,0,size,size);
    glScissor(0,0,size,size);

    // The Z-buffer is cleared to make the axis appear over the
    // original image.
    glClear(GL_DEPTH_BUFFER_BIT);

    // Tune for best line rendering
    glDisable(GL_LIGHTING);
    glLineWidth(2.0);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(-1, 1, -1, 1, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glMultMatrixd(camera()->orientation().inverse().matrix());

    glBegin(GL_LINES);
    glColor3f(1.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(axisLength, 0.0, 0.0);

    glColor3f(0.0, 1.0, 0.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, axisLength, 0.0);

    glColor3f(0.0, 0.0, 1.0);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, axisLength);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable(GL_LIGHTING);

    // The viewport and the scissor are restored.
    glScissor(scissor[0],scissor[1],scissor[2],scissor[3]);
    glViewport(viewport[0],viewport[1],viewport[2],viewport[3]);
}
//!
//! \brief Viewer::drawPoint
//! \param x
//! \param y
//! \param z
//! \param radius
//! \param subdivisions
//! \param quadric
//! This function draws a point in space
void Viewer::drawPoint(GLfloat x, GLfloat y, GLfloat z, GLfloat radius, GLint subdivisions, GLUquadric *quadric)
{
    glPushMatrix();
    glTranslatef( x,y,z );
   // gluSphere(quadric, radius, subdivisions,subdivisions);
    glVertex3f(x,y,z);
    glPopMatrix();

}
//!
//! \brief Viewer::draw
//! This function draws all points
void Viewer::draw()
{

if(!updatingPoints){
    glBegin(GL_POINTS);
    GLUquadricObj *quadric=gluNewQuadric();

    gluQuadricNormals(quadric, GLU_SMOOTH);

        for (int p=0; p<allPoints.size(); ++p)
        {

            //      float angle = 2.0*ratio;
            //	  float c = cos(angle);
            //	  float s = sin(angle);
            //      float r1 = 1.0 - 0.8*ratio;
            //      float r2 = 0.8- 0.8*ratio;
            //	  float alt = ratio - 0.5;
            //	  const float nor = .5;
            //	  const float up = sqrt(1.0-nor*nor);

            //      glNormal3f(nor*c, up, nor*s);

            //	  glVertex3f(r1*c, alt, r1*s);
            //      glVertex3f(r2*c, alt+0.05, r2*s);

            PointCluster* pc=allPoints[p];
//            float ratio = (float)p/allPoints.size();
//            glColor3f(1.0-ratio, 0.2f , ratio);
            for(int i=0;i<pc->getCount();i++){
                drawPoint( *(*(pc->getPoints()+i)),*(*(pc->getPoints()+i)+1),*(*(pc->getPoints()+i)+2),0.01,5,quadric);
            }
        }

    gluDeleteQuadric(quadric);
    glEnd();
    }
}

//!
//! \brief Viewer::postDraw
//! Post drawing routine
void Viewer::postDraw()
{
    QGLViewer::postDraw();
    drawCornerAxis();
}

QString Viewer::helpString() const
{
    QString text("<h2>3D Reconstruction of Psoriasis lesions</h2>");
    text += "More help text goes here. ";

    return text;
}


