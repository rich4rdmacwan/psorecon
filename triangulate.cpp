#include <cv.h>
#include <string>
#include <iostream>
#include<highgui.h>
using namespace cv;
Mat prepareImage(char* img,int flipArg){
  Mat imgMat=imread(img);
  transpose(imgMat,imgMat);
  flip(imgMat,imgMat,flipArg);
  //resize(imgMat,imgMat,Size(imgMat.cols/4,imgMat.rows/4));
  
  return imgMat;
}
int main(int argc, char* argv[]){
  //TODO: Load a stereo pair
  char* img="psoriasis_images/cam_01/IMAGE_0001.JPG";
  char* imgmatch1="psoriasis_images/cam_01/IMAGE_0011.JPG";
  char* imgmatch2="psoriasis_images/cam_01/IMAGE_0012.JPG";
  Mat imgMat=prepareImage(img,1);
  Mat imgMatMatch1=prepareImage(imgmatch2,2);
  imshow("",imgMatMatch1);
  
  
  //TODO: Apply SIFT on both
  //TODO: Find matches
  //TODO: Triangulate
  waitKey(0);
  return 0;
}