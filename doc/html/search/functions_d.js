var searchData=
[
  ['setcount',['setCount',['../class_point_cluster.html#a27721956b257aa8bc334e17eca20c649',1,'PointCluster']]],
  ['setimages',['setImages',['../class_image_viewer.html#a3cf00f2fd240b886b4745941f32881ce',1,'ImageViewer::setImages(QStringList images)'],['../class_image_viewer.html#a648492560e2415e156ef32e08a9db6b1',1,'ImageViewer::setImages(std::vector&lt; cv::Mat &gt; images)']]],
  ['setpixmap',['setPixmap',['../class_aspect_ratio_pixmap_label.html#ae731b8cb4f304432971625db6436126c',1,'AspectRatioPixmapLabel']]],
  ['setpoints',['setPoints',['../class_point_cluster.html#af1a426c7479388671d6de1970c4075c3',1,'PointCluster']]],
  ['setupui',['setupUi',['../class_ui___image_viewer.html#a3ccf73661022e411159f67021597cfe6',1,'Ui_ImageViewer::setupUi()'],['../class_ui___dialog.html#a4f6a478c3ecdafabffb17b39cb26444a',1,'Ui_Dialog::setupUi()']]],
  ['sizehint',['sizeHint',['../class_aspect_ratio_pixmap_label.html#a6931cb5bb7fa8fc910faf4e336b55746',1,'AspectRatioPixmapLabel']]]
];
