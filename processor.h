#ifndef PROCESSOR_H
#define PROCESSOR_H
#include "calibextr.h"
#include <vector>
#include <QObject>
#include <QRunnable>
#include <QThreadPool>
#include <pointcluster.h>
#include <imageviewer.h>
class Processor : public QObject,QRunnable
{
    Q_OBJECT
public:
    explicit Processor(QObject *parent = 0);
    void init();
    void process();
    void run();
    CalibExtr* calibExtr;
    GLfloat** points;
    int nPoints;
    QStringList getStereoPairImages(int);
Q_SIGNALS:
    void addNewPointCluster(PointCluster *pointCluster);
private:
    /*
     * Need to configure this path while installation
     */

ImageViewer imgViewer;
std::vector<cv::Mat> imageMats,imageMatsGray;
void preprocessImages(int i, int j);
void matchPairs(cv::Mat& img1, cv::Mat& img2);
};

#endif // PROCESSOR_H
