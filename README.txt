Dependencies
------------
--Matlab
--QGlViewer (for displaying the 3D points)
--OpenCV 
--Qt
--CMVS-PMVS for extracting 3D points from the images.

Folder Structure
-----------------
Since the image sizes are quite large, they have not been added to the project repository. 

---------
IMPORTANT: The paths to the binary of pmvs and path to the images need to be set in callingibextr.h in the variables "pmvsBinPath" and "imagePath" respectively.
---------  

The results will be stored in the folder "pmvsData/models" in the project folder.



Instructions for compiling
--------------------------
This program makes use of Matlab's C++ api to parse data from .mat files. For compiling it correctly, the environment needs to set up for accessing matlab libraries.
This can be done by running the command:
`env PATH=$PATH:/opt/MATLAB/R2013a/bin LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/MATLAB/R2013a/bin/glnxa64 xterm`
which launches a terminal with the appropriate environment set up. (On the development machine, this command can be invoked by calling xterm-matlab or pressing the "Windows+M" key combination)

After this, one needs to change the directory to the path of the software (/home/richard/Documents/Internship/3DReconstruction/psorecon in the dev machine) and execute:qmake && make && ./psorecon 
to compile and run the program.

Cross-platform
--------------
The .pro file is configured to run on MAC and Windows platforms, but hasn't been tested on those. Setting the correct paths in the .pro file and installing the 
prerequisites should get it going.

Some notes on debugging
-----------------------
--Sometimes on the developer machine an error due to ".ICEAuthority policy" halts the compilation. In this case, just delete the .ICEAuthority file:
	rm $HOME/.ICEAuthority

Documentation
-------------
See Documentation.html

Author: 
Richard Macwan
rich4rd.macwan@gmail.com