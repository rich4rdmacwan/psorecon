#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
#include <QPixmap>
#include <QDialog>
#include <vector>
#include <cv.h>
#include "asmOpenCV.h"
namespace Ui {
class ImageViewer;
}

class ImageViewer : public QDialog
{
    Q_OBJECT

public:
    explicit ImageViewer(QWidget *parent = 0);
    ~ImageViewer();
     Q_DECL_DEPRECATED void setImages(QStringList images);
     void setImages(std::vector<cv::Mat> images);
     void updateView();
     void resizeEvent(QResizeEvent *);
private:
    Ui::ImageViewer *ui;
    std::vector<QPixmap> qpixmaps;


};

#endif // IMAGEVIEWER_H
